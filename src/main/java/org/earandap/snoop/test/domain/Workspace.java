package org.earandap.snoop.test.domain;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by eduardo on 4/1/2015.
 */
public class Workspace {

    private long id;
    private String nombre;
    private int limiteFiguras;
    private List<Figura> figuras;

    public Workspace() {
        this(null, 0);
    }

    public Workspace(String nombre, int limiteFiguras) {
        this.nombre = nombre;
        this.limiteFiguras = limiteFiguras;
        this.figuras = new ArrayList<>();

    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getLimiteFiguras() {
        return limiteFiguras;
    }

    public void setLimiteFiguras(int limiteFiguras) {
        this.limiteFiguras = limiteFiguras;
    }

    public List<Figura> getFiguras() {
        return figuras;
    }

    public void setFiguras(List<Figura> figuras) {
        this.figuras = figuras;
    }

    public boolean agregarFigura(Figura figura) {
        if (figuras.size() < limiteFiguras) {
            figuras.add(figura);
            return true;
        }
        return false;
    }



    public boolean isLleno() {
        return figuras.size() == limiteFiguras;
    }

    public double getAreaTotal() {

        double total = 0;
        for (Figura figura : figuras) {
            total += figura.getArea();
        }
        return total;
    }

    public double getApotemaTotal() {
        double total = 0;
        for (Figura figura : figuras) {
            if (figura instanceof Hexagono)
                total += ((Hexagono) figura).getApotema();
        }
        return total;
    }

    public void cambiarFigura(Figura vieja, Figura nueva) {
        int posicion = figuras.indexOf(vieja);
        if (posicion != -1)
            figuras.set(posicion, nueva);
    }

}
