package org.earandap.snoop.test.domain;

import java.util.Map;

/**
 * Created by eduardo on 4/1/2015.
 */
public class Hexagono extends Figura {

    private double radio;

    public Hexagono(){
        this(0);
    }
    public Hexagono(double radio) {
        super(6);
        this.radio = radio;
    }

    @Override
    public double getArea() {
        return getPerimetro() * getApotema() / 2;
    }

    @Override
    public double getPerimetro() {
        return 6 * radio;
    }

    public double getApotema() {
        return Math.sqrt(Math.pow(radio, 2) - Math.pow(radio / 2d, 2));
    }

    public double getRadio() {
        return radio;
    }

    public void setRadio(double radio) {
        this.radio = radio;
    }
}
