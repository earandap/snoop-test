package org.earandap.snoop.test.domain;

/**
 * Created by eduardo on 4/1/2015.
 */
public class Triangulo extends Figura {

    private int base;

    private int altura;

    private int hipotenusa;

    public Triangulo() {
        this(0, 0, 0);

    }

    public Triangulo(int base, int altura, int hipotenusa) {
        super(3);
        this.base = base;
        this.altura = altura;
        this.hipotenusa = hipotenusa;
    }

    public int getBase() {
        return base;
    }

    public void setBase(int base) {
        this.base = base;
    }

    public int getAltura() {
        return altura;
    }

    public void setAltura(int altura) {
        this.altura = altura;
    }

    public int getHipotenusa() {
        return hipotenusa;
    }

    public void setHipotenusa(int hipotenusa) {
        this.hipotenusa = hipotenusa;
    }

    @Override
    public double getArea() {
        return base * altura / 2;
    }

    @Override
    public double getPerimetro() {
        return base + hipotenusa + altura;
    }
}
