package org.earandap.snoop.test.controller;

import org.earandap.snoop.test.dao.WorkspaceDAO;
import org.earandap.snoop.test.domain.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

/**
 * Created by eduardo on 4/1/2015.
 */
@Controller
public class WorkspaceController {

    @Autowired
    private WorkspaceDAO workspaceDAO;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String index() {
        return "redirect:list";
    }

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public String list(Model model, @RequestParam(value = "estado", required = false, defaultValue = "false") boolean estado) {
        model.addAttribute("workspaces", workspaceDAO.listar());
        if (estado)
            model.addAttribute("mensaje", "Workspace creado correctamente");
        return "list";
    }

    @RequestMapping(value = "/workspace", method = RequestMethod.GET)
    public String crearWorkspace(Model model) {
        model.addAttribute("titulo", "Nuevo workspace");
        model.addAttribute("workspace", new Workspace());
        return "workspace";
    }

    @RequestMapping(value = "/workspace/{id}", method = RequestMethod.GET)
    public String editarWorkspace(Model model, @PathVariable("id") long id) {

        Workspace workspace = workspaceDAO.obtener(id);
        model.addAttribute("titulo", "Editar workspace: " + workspace.getNombre());
        model.addAttribute("workspace", workspace);
        return "workspace";
    }

    @RequestMapping(value = "/workspace", method = RequestMethod.POST)
    public String submitWorkspace(@ModelAttribute Workspace workspace) {
        Workspace w = workspaceDAO.obtener(workspace.getId());
        if (w != null)
            workspace.setFiguras(w.getFiguras());
        workspaceDAO.guardar(workspace);
        return "redirect:list?estado=true";
    }

    @RequestMapping(value = "/eliminar/workspace/{id}", method = RequestMethod.GET)
    public String eliminarWorkspace(@PathVariable("id") long id) {
        workspaceDAO.eliminar(id);
        return "redirect:../../list?estado=true";
    }

    @RequestMapping(value = "/figura", method = RequestMethod.GET)
    public String crearFigura(Model model, @RequestParam String type, @RequestParam long workspaceId) {
        model.addAttribute("titulo", "Nueva figura");
        Figura figura = null;
        if (type.equals("cuadrado"))
            figura = new Cuadrado();
        else if (type.equals("triangulo"))
            figura = new Triangulo();
        else
            figura = new Hexagono();

        model.addAttribute("figura", figura);
        model.addAttribute("workspaceId", workspaceId);
        return "add-figura";
    }

    @RequestMapping(value = "/c-figura", method = RequestMethod.GET)
    public String cambiarFigura(Model model, @RequestParam String type, @RequestParam long workspaceId, @RequestParam int posicion) {
        model.addAttribute("titulo", "Nueva figura");
        Figura figura;
        if (type.equals("cuadrado"))
            figura = new Cuadrado();
        else if (type.equals("triangulo"))
            figura = new Triangulo();
        else
            figura = new Hexagono();

        model.addAttribute("posicion", posicion);
        model.addAttribute("figura", figura);
        model.addAttribute("workspaceId", workspaceId);
        return "change-figura";
    }

    @RequestMapping(value = "/cuadrado", method = RequestMethod.POST)
    public String submitCuadrado(@ModelAttribute Cuadrado cuadrado, @RequestParam int workspaceId, Model model) {

        Workspace workspace = workspaceDAO.obtener(workspaceId);
        boolean resultado = workspace.agregarFigura(cuadrado);
        String r = "ok";
        if (!resultado)
            r = "error";

        return "redirect:figura/" + workspaceId + "?r=" + r;
    }

    @RequestMapping(value = "/c-cuadrado", method = RequestMethod.POST)
    public String submitCambiarCuadrado(@ModelAttribute Cuadrado cuadrado, @RequestParam int workspaceId, @RequestParam int posicion, Model model) {

        Workspace workspace = workspaceDAO.obtener(workspaceId);
        Figura vieja = workspace.getFiguras().get(posicion);
        workspace.cambiarFigura(vieja, cuadrado);

        return "redirect:figura/" + workspaceId;
    }

    @RequestMapping(value = "/triangulo", method = RequestMethod.POST)
    public String submitTriangulo(@ModelAttribute Triangulo triangulo, @RequestParam int workspaceId) {
        Workspace workspace = workspaceDAO.obtener(workspaceId);
        boolean resultado = workspace.agregarFigura(triangulo);
        String r = "ok";
        if (!resultado)
            r = "error";

        return "redirect:figura/" + workspaceId + "?r=" + r;
    }

    @RequestMapping(value = "/c-triangulo", method = RequestMethod.POST)
    public String submitCambiarTriangulo(@ModelAttribute Triangulo figura, @RequestParam int workspaceId, @RequestParam int posicion, Model model) {

        Workspace workspace = workspaceDAO.obtener(workspaceId);
        Figura vieja = workspace.getFiguras().get(posicion);
        workspace.cambiarFigura(vieja, figura);

        return "redirect:figura/" + workspaceId;
    }

    @RequestMapping(value = "/hexagono", method = RequestMethod.POST)
    public String submitHexagono(@ModelAttribute Hexagono hexagono, @RequestParam int workspaceId) {
        Workspace workspace = workspaceDAO.obtener(workspaceId);
        boolean resultado = workspace.agregarFigura(hexagono);
        String r = "ok";
        if (!resultado)
            r = "error";

        return "redirect:figura/" + workspaceId + "?r=" + r;
    }

    @RequestMapping(value = "/c-hexagono", method = RequestMethod.POST)
    public String submitCambiarHexagono(@ModelAttribute Hexagono figura, @RequestParam int workspaceId, @RequestParam int posicion, Model model) {

        Workspace workspace = workspaceDAO.obtener(workspaceId);
        Figura vieja = workspace.getFiguras().get(posicion);
        workspace.cambiarFigura(vieja, figura);

        return "redirect:figura/" + workspaceId;
    }


    @RequestMapping(value = "/figura/{id}", method = RequestMethod.GET)
    public String figura(@PathVariable("id") long id, @RequestParam(required = false) String r, Model model) {
        Workspace workspace = workspaceDAO.obtener(id);
        model.addAttribute("workspace", workspace);
        if (r != null) {
            model.addAttribute("r", r);
        }
        return "list-figuras";
    }


}
