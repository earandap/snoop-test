package org.earandap.snoop.test;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SnoopTestApplication {

    public static void main(String[] args) {
        SpringApplication.run(SnoopTestApplication.class, args);
    }
}
